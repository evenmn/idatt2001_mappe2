package patientApp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Screen;

import java.io.IOException;
import java.util.logging.Logger;


public class PatientApp extends Application {

    private final String APP_TITLE = "Patient Registry";

    private Logger logger = Logger.getLogger(this.getClass().getName());

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MainPage.fxml"));
        try {
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
        } catch (IOException e) {
            logger.severe("ERROR: An IO-exception occurred. Cause: "
                + e.getCause());
            throw e;
        }

        primaryStage.setTitle(APP_TITLE);
        primaryStage.setOnCloseRequest(e -> System.exit(0));

        Rectangle2D screen = Screen.getPrimary().getVisualBounds();
        primaryStage.setWidth(screen.getWidth());
        primaryStage.setHeight(screen.getHeight());
        primaryStage.setMaximized(true);

        primaryStage.show();
    }
}

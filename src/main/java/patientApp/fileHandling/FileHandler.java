package patientApp.fileHandling;

import patientApp.model.Patient;
import patientApp.model.PatientRegistry;

import java.io.*;

public class FileHandler {

    /**
     * Writes patient registry to file in a .csv format.
     * @param file Target file to write in.
     * @param registry Patient registry that is to be saved.
     */
    public static void writeToFile(File file, PatientRegistry registry) {
        try (PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(file)))) {
            for (Patient patient : registry.getPatientRegistry()) {
                printWriter.println(patient.getFirstName() + "," + patient.getLastName() + "," + patient.getSocialSecurityNumber() + "," +
                        patient.getDiagnosis() + "," + patient.getGeneralPractitioner());
                printWriter.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reads file in .csv format creates patient objects.
     * Adds patients to patient registry.
     * @param file File that is to be read.
     * @return Registry read from file.
     */
    public static PatientRegistry readFromFile(File file) {
        PatientRegistry registry = new PatientRegistry();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                String[] values = line.split(",");
                try {
                    registry.addPatient(new Patient(values[0], values[1], values[2], values[3], values[4]));
                } catch (Exception e) {
                    System.out.println("File contains duplicates of same patient");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return registry;
    }
}

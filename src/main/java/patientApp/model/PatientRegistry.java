package patientApp.model;

import patientApp.model.Patient;

import java.util.ArrayList;

public class PatientRegistry {

    ArrayList<Patient> patientRegistry;

    public PatientRegistry() {
        patientRegistry = new ArrayList<>();
    }

    /**
     * Adds new patient to registry.
     * @param patient Patient that is to be added.
     * @throws Exception Throws exception if patient already exists in registry.
     */
    public void addPatient(Patient patient) throws Exception {
        if (!patientRegistry.contains(patient)) {
            patientRegistry.add(patient);
        } else {
            throw new Exception("This patient already exists in this registry");
        }
    }

    /**
     * Removes patient from registry.
     * @param patient Patient that is to be removed.
     * @throws Exception Throws exception if patient does not exist in registry.
     */
    public void removePatient(Patient patient) throws Exception {
        if (patientRegistry.contains(patient)) {
            patientRegistry.remove(patient);
        } else {
            throw new Exception("This patient does not exist in this registry");
        }
    }

    public ArrayList<Patient> getPatientRegistry() {
        return patientRegistry;
    }

}

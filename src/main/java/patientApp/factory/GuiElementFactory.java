package patientApp.factory;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class GuiElementFactory {

    /**
     * Creates new button.
     * @param title Button label.
     * @return New button.
     */
    public Button createButton(String title) {
        return  new Button(title);
    }

    /**
     * Creates new VBox.
     * @param spacing Spacing between elements in VBox.
     * @return New VBox
     */
    public VBox createVBox(double spacing) {
        return new VBox(spacing);
    }

    /**
     * Creates new HBox
     * @param spacing Spacing between elements in HBox.
     * @return New HBox.
     */
    public HBox createHBox(double spacing) {
        return new HBox(spacing);
    }

    /**
     * Creates new label.
     * @param title Text in label.
     * @return New label.
     */
    public Label createLabel(String title) {
        return new Label(title);
    }

    /**
     * Creates new text field
     * @param text Text to be displayed.
     * @return New text field.
     */
    public TextField createTextField(String text) {
        return new TextField(text);
    }

    /**
     * Creates new border pane.
     * @return new border pane.
     */
    public BorderPane createBorderPane() {
        return new BorderPane();
    }

    /**
     * Creates new grid pane.
     * @return New grid pane.
     */
    public GridPane createGridPane() {
        return new GridPane();
    }
}

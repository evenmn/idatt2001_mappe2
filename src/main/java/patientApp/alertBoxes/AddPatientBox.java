package patientApp.alertBoxes;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import patientApp.factory.GuiElementFactory;

import java.util.ArrayList;

public class AddPatientBox {

    static ArrayList<String> fieldInputs;

    /**
     * Displays a window where user can input patient data.
     * Will not return if any field is empty.
     * @return A list of strings from user input.
     * In order: first name, last name, social security number, diagnosis, general practitioner.
     */
    public static ArrayList<String> display() {
        fieldInputs = new ArrayList<>();
        GuiElementFactory factory = new GuiElementFactory();

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Add Patient");
        window.setWidth(500);
        window.setHeight(350);
        window.setResizable(false);

        BorderPane layout = factory.createBorderPane();
        layout.setPadding(new Insets(20, 20, 20, 10));

        Label message = factory.createLabel("Fill in all fields to add patient: ");
        layout.setTop(message);

        GridPane gridPane = factory.createGridPane();
        gridPane.setPadding(new Insets(20, 10, 10, 30));
        gridPane.setVgap(8);
        gridPane.setHgap(10);


        //Create input fields
        TextField firstNameField = factory.createTextField(null);
        firstNameField.setPromptText("First Name");
        Label firstNameLabel = factory.createLabel("First Name: ");
        GridPane.setConstraints(firstNameLabel, 0, 1);
        GridPane.setConstraints(firstNameField, 1, 1);

        TextField lastNameField = factory.createTextField(null);
        lastNameField.setPromptText("Last Name");
        Label lastNameLabel = factory.createLabel("Last Name: ");
        GridPane.setConstraints(lastNameLabel, 0, 2);
        GridPane.setConstraints(lastNameField, 1, 2);

        TextField socialSecurityField = factory.createTextField(null);
        socialSecurityField.setPromptText("Social Security Number");
        Label socialSecurityLabel = factory.createLabel("Social Security Number: ");
        GridPane.setConstraints(socialSecurityLabel, 0, 3);
        GridPane.setConstraints(socialSecurityField, 1, 3);

        TextField diagnosisField = factory.createTextField(null);
        diagnosisField.setPromptText("Diagnosis");
        Label diagnosisLabel = factory.createLabel("Diagnosis: ");
        GridPane.setConstraints(diagnosisLabel, 0, 4);
        GridPane.setConstraints(diagnosisField, 1, 4);

        TextField generalPractitionerField = factory.createTextField(null);
        generalPractitionerField.setPromptText("General Practitioner");
        Label generalPractitionerLabel = factory.createLabel("General Practitioner: ");
        GridPane.setConstraints(generalPractitionerLabel, 0, 5);
        GridPane.setConstraints(generalPractitionerField, 1, 5);

        gridPane.getChildren().addAll(firstNameLabel, firstNameField, lastNameLabel, lastNameField,
                socialSecurityLabel, socialSecurityField, diagnosisLabel, diagnosisField, generalPractitionerLabel,
                generalPractitionerField);

        layout.setCenter(gridPane);


        //Create confirm- and cancel button
        Button okButton = factory.createButton("OK");
        okButton.setOnAction(e -> {
            if (!firstNameField.getText().isEmpty() && !lastNameField.getText().isEmpty()
                    && !socialSecurityField.getText().isEmpty() && !diagnosisField.getText().isEmpty()
                    && !generalPractitionerField.getText().isEmpty()) {
                fieldInputs.add(firstNameField.getText());
                fieldInputs.add(lastNameField.getText());
                fieldInputs.add(socialSecurityField.getText());
                fieldInputs.add(diagnosisField.getText());
                fieldInputs.add(generalPractitionerField.getText());
                window.close();
            }
        });

        Button cancelButton = factory.createButton("Cancel");
        cancelButton.setOnAction(e -> window.close());

        HBox buttonLayout = factory.createHBox(10);
        buttonLayout.setAlignment(Pos.CENTER);
        buttonLayout.getChildren().addAll(okButton, cancelButton);
        layout.setBottom(buttonLayout);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        return fieldInputs;
    }
}

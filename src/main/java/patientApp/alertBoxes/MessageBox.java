package patientApp.alertBoxes;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import patientApp.factory.GuiElementFactory;


public class MessageBox {

    /**
     * Displays a message box and waits for user to exit.
     * @param message Message that is to be displayed.
     */
    public static void display(String message) {
        GuiElementFactory factory = new GuiElementFactory();

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Message");
        window.setWidth(300);
        window.setHeight(150);
        window.setResizable(false);

        Label label = factory.createLabel(message);

        Button okButton = factory.createButton("OK");
        okButton.setOnAction(e -> window.close());

        VBox layout = factory.createVBox(20);
        layout.setPadding(new Insets(10, 10, 10, 10));
        layout.setAlignment(Pos.CENTER_LEFT);

        layout.getChildren().addAll(label, okButton);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
    }
}

package patientApp.alertBoxes;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import patientApp.factory.GuiElementFactory;


public class InformationBox {

    /**
     * Displays a window containing information about the application, and waits for user exit.
     */
    public static void display() {
        GuiElementFactory factory = new GuiElementFactory();

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("About");
        window.setWidth(300);
        window.setHeight(200);
        window.setResizable(false);

        Label mainLabel = factory.createLabel("Patient Register");
        Label versionLabel = factory.createLabel("v0.1-SNAPSHOT");
        VBox titleBox = factory.createVBox(0);
        titleBox.getChildren().addAll(mainLabel, versionLabel);

        Label creatorLabel = factory.createLabel("Created by");
        Label nameLabel = factory.createLabel("Even Malm Nordgaard");
        VBox creatorBox = factory.createVBox(0);
        creatorBox.getChildren().addAll(creatorLabel, nameLabel);

        Button okButton = factory.createButton("OK");
        okButton.setOnAction(e -> window.close());

        VBox layout = factory.createVBox(20);
        layout.setPadding(new Insets(20, 20, 20, 20));
        layout.setAlignment(Pos.CENTER_LEFT);

        layout.getChildren().addAll(titleBox, creatorBox, okButton);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
    }
}

package patientApp.alertBoxes;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import patientApp.factory.GuiElementFactory;

public class DeleteConfirmationBox {

    static boolean answer;

    /**
     * Displays window asking user if they want to delete patient.
     * @return True if user chooses to delete. False if not.
     */
    public static boolean display() {

        GuiElementFactory factory = new GuiElementFactory();

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Delete Confirmation");
        window.setWidth(400);
        window.setHeight(130);
        window.setResizable(false);

        Label label = factory.createLabel("Are you sure you want to delete this item?");

        Button okButton = factory.createButton("OK");
        okButton.setOnAction(e -> {
            answer = true;
            window.close();
        });

        Button cancelButton = factory.createButton("Cancel");
        cancelButton.setOnAction(e -> {
            answer = false;
            window.close();
        });

        VBox layout = factory.createVBox(30);
        layout.setAlignment(Pos.CENTER);
        HBox buttonLayout = factory.createHBox(10);

        buttonLayout.setAlignment(Pos.CENTER);
        buttonLayout.getChildren().addAll(okButton, cancelButton);
        layout.getChildren().addAll(label, buttonLayout);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        return answer;
    }
}

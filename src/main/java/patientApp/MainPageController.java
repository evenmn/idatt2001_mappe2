package patientApp;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import patientApp.alertBoxes.*;
import patientApp.fileHandling.FileHandler;
import patientApp.model.Patient;
import patientApp.model.PatientRegistry;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class MainPageController implements Initializable {

    private PatientRegistry patientRegistry;
    private ObservableList<Patient> patientObservableList;
    private FileChooser fileChooser;

    @FXML private TableView<Patient> tableView;
    @FXML private TableColumn<Patient, String> firstNameColumn;
    @FXML private TableColumn<Patient, String> lastNameColumn;
    @FXML private TableColumn<Patient, String> socialSecurityColumn;
    @FXML private TableColumn<Patient, String> diagnosisColumn;
    @FXML private TableColumn<Patient, String> generalPractitionerColumn;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.patientRegistry = new PatientRegistry();

        this.firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        this.lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        this.socialSecurityColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        this.diagnosisColumn.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        this.generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));

        this.patientObservableList = FXCollections.observableArrayList(this.patientRegistry.getPatientRegistry());

        this.fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir") + "/Saved Files"));

        this.tableView.setItems(patientObservableList);
    }

    /**
     * Updates the observable list to be equal to the current patient registry.
     * This in turn updates the table view.
     */
    public void updateObservableList() {
        this.patientObservableList.setAll(this.patientRegistry.getPatientRegistry());
    }

    /**
     * Removes selected patient from registry and updates the table view.
     * @param mouseEvent Click on remove patient image in toolbar.
     */
    public void removePatient(MouseEvent mouseEvent) {
        if (tableView.getSelectionModel().getSelectedItem() != null) {
            if (DeleteConfirmationBox.display()) {
                try {
                    patientRegistry.removePatient(tableView.getSelectionModel().getSelectedItem());
                    updateObservableList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Removes selected patient from registry and updates the table view.
     * @param actionEvent Click on remove patient in menubar.
     */
    public void removePatientByMenu(ActionEvent actionEvent) {
        if (tableView.getSelectionModel().getSelectedItem() != null) {
            if (DeleteConfirmationBox.display()) {
                try {
                    patientRegistry.removePatient(tableView.getSelectionModel().getSelectedItem());
                    updateObservableList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Opens add patient window and uses input to create new patient.
     * Adds new patient to the patient registry and updates the table view.
     * @param mouseEvent Click on add patient image in toolbar.
     */
    public void addPatient(MouseEvent mouseEvent) {
        ArrayList<String> fields = AddPatientBox.display();
        addPatientFromArray(fields);
    }

    /**
     * Opens add patient window and uses input to create new patient.
     * Adds new patient to the patient registry and updates the table view.
     * @param actionEvent Click on add patient in menubar.
     */
    public void addPatientByMenu(ActionEvent actionEvent) {
        ArrayList<String> fields = AddPatientBox.display();
        addPatientFromArray(fields);
    }

    /**
     * Takes array from user input uses data to create new patient and add to registry.
     * @param fields Array containing patient data.
     */
    public void addPatientFromArray(ArrayList<String> fields) {
        if (fields.size() == 5) {
            try {
                Patient patient = new Patient(fields.get(0), fields.get(1), fields.get(2), fields.get(3), fields.get(4));
                patientRegistry.addPatient(patient);
                updateObservableList();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Opens edit patient window for selected patient and uses input to edit patient information.
     * Updates the table view.
     * @param mouseEvent Click on 'edit patient' image in toolbar.
     */
    public void editPatient(MouseEvent mouseEvent) {
        if (tableView.getSelectionModel().getSelectedItem() != null) {
            Patient patient = tableView.getSelectionModel().getSelectedItem();
            ArrayList<String> fields = EditPatientBox.display(patient);
            editPatientFromArray(fields, patient);
        }
    }

    /**
     * Opens edit patient window for selected patient and uses input to edit patient information.
     * Updates the table view.
     * @param actionEvent Click on 'edit patient' in menubar.
     */
    public void editPatientByMenu(ActionEvent actionEvent) {
        if (tableView.getSelectionModel().getSelectedItem() != null) {
            Patient patient = tableView.getSelectionModel().getSelectedItem();
            ArrayList<String> fields = EditPatientBox.display(patient);
            editPatientFromArray(fields, patient);
        }
    }

    /**
     * Takes array from user input and updates patient information.
     * @param fields Array containing patient data.
     * @param patient Patient that is to be edited.
     */
    public void editPatientFromArray(ArrayList<String> fields, Patient patient) {
        if (fields.size() == 5) {
            patient.setFirstName(fields.get(0));
            patient.setLastName(fields.get(1));
            patient.setSocialSecurityNumber(fields.get(2));
            patient.setDiagnosis(fields.get(3));
            patient.setGeneralPractitioner(fields.get(4));
            updateObservableList();
        }
    }

    /**
     * Displays information window about the application.
     * @param actionEvent Click on 'About' in menubar.
     */
    public void showAboutWindow(ActionEvent actionEvent) {
        InformationBox.display();
    }

    /**
     * Opens file chooser and lets user pick a .csv file to import to current list.
     * Updates registry and table view.
     * @param actionEvent Click on 'Import from .CSV in menubar.
     */
    public void importFile(ActionEvent actionEvent)  {
        Window stage = this.tableView.getScene().getWindow();
        fileChooser.setTitle("Load file");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(".csv file", "*.csv"));

        File file = fileChooser.showOpenDialog(stage);
        if (file == null) {
            return;
        }

        //This code checks if file not a .csv file.
        //Since the extension filter has been set to '*.csv' this code isn't strictly necessary
        //It's been included because of the problem description
        if (!getFileExtension(file.getName()).equals("csv")) {
            MessageBox.display("This file format is not supported");
        } else {
            if (file.exists()) {
                this.patientRegistry = FileHandler.readFromFile(file);
                updateObservableList();
            }
        }
    }

    /**
     * Opens file chooser and lets user save current list to a .csv file.
     * @param actionEvent Click on 'Export to .CSV' in menubar.
     */
    public void exportFile(ActionEvent actionEvent) {
        Window stage = this.tableView.getScene().getWindow();
        fileChooser.setTitle("Save file");
        fileChooser.setInitialFileName("PatientRegistrySave");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(".csv file", "*.csv"));

        File file = fileChooser.showSaveDialog(stage);

        if (file != null) {
            if (!getFileExtension(file.getName()).equals("csv")) {
                MessageBox.display("This file format is not supported. \nPlease save as .csv file");
            } else {
                    FileHandler.writeToFile(file, this.patientRegistry);
            }
        }
    }


    /**
     * Exits the application and terminates program.
     * @param actionEvent Click on 'Exit' in menubar.
     */
    public void exit(ActionEvent actionEvent) {
        System.exit(0);
    }

    /**
     * Returns extension of a file as a String.
     * For example a .csv file would return 'csv'.
     * @param fileName Name of the file that is to be checked.
     * @return File extension.
     */
    public String getFileExtension (String fileName) {
        String extension = "";
        int i = fileName.lastIndexOf('.');
        if (i >= 0) {
            extension = fileName.substring(i+1);
        }
        return extension;
    }
}

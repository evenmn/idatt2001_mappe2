import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import patientApp.PatientApp;
import patientApp.fileHandling.FileHandler;
import patientApp.model.Patient;
import patientApp.model.PatientRegistry;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FileHandlerTest {

    PatientRegistry registry;
    URL url;
    File testData;
    File emptyFile;

    @BeforeEach
    void setUp() {
        registry = new PatientRegistry();
        url = PatientApp.class.getClassLoader().getResource("dataFiles/");
        assert url != null;
        testData = new File(url.getPath() + "TestData.csv");
        emptyFile = new File(url.getPath() + "test.csv");

        try {
            if (emptyFile.createNewFile()) {
                System.out.println("S U C C E S S");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(url.getPath());

    }

    @Test
    void loadTestData() {
        registry = FileHandler.readFromFile(testData);
        assertEquals(registry.getPatientRegistry().size(), 2);
        assertEquals(registry.getPatientRegistry().get(0).getFirstName(), "Ola");
        assertEquals(registry.getPatientRegistry().get(1).getFirstName(), "Frida");
    }

    @Test
    void saveAndThenLoadTestData() {
        try {
            registry.addPatient(new Patient("Teodor", "Strøm", "2222", "Hodepine", "Karl Sørdahl"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        FileHandler.writeToFile(emptyFile, registry);
        registry.getPatientRegistry().clear();
        assertTrue(registry.getPatientRegistry().isEmpty());

        registry = FileHandler.readFromFile(emptyFile);
        assertEquals(registry.getPatientRegistry().size(), 1);
        assertEquals(registry.getPatientRegistry().get(0).getFirstName(), "Teodor");
    }
}

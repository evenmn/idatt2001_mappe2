import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import patientApp.model.Patient;
import patientApp.model.PatientRegistry;

import static org.junit.jupiter.api.Assertions.*;

public class ModelTest {

    Patient patient1;
    Patient patient2;
    PatientRegistry registry;

    @BeforeEach
    void setUp() {
        patient1 = new Patient("Ola", "Nordmann", "0000", "Forkjølelse", "Harald Bø");
        patient2 = new Patient("Frida", "Ness", "4444", "Lungebetennelse", "Torild Nordland");
        registry = new PatientRegistry();
    }

    @Test
    void patientGetters() {
        assertEquals(patient1.getFirstName(), "Ola");
        assertEquals(patient1.getLastName(), "Nordmann");
        assertEquals(patient1.getSocialSecurityNumber(), "0000");
        assertEquals(patient1.getDiagnosis(), "Forkjølelse");
        assertEquals(patient1.getGeneralPractitioner(), "Harald Bø");
    }

    @Test
    void patientSetters() {
        patient1.setDiagnosis("Hodepine");
        patient1.setLastName("Staale");

        assertEquals(patient1.getLastName(), "Staale");
        assertEquals(patient1.getDiagnosis(), "Hodepine");
    }

    @Test
    void addAndRemovePatientsFromRegistry() {
        assertEquals(registry.getPatientRegistry().size(), 0);

        try {
            registry.addPatient(patient1);
            registry.addPatient(patient2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(registry.getPatientRegistry().size(), 2);
        assertTrue(registry.getPatientRegistry().contains(patient1));
        assertTrue(registry.getPatientRegistry().contains(patient2));

        try {
            registry.removePatient(patient1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(registry.getPatientRegistry().size(), 1);
        assertFalse(registry.getPatientRegistry().contains(patient1));
    }

    @Test
    void addExistingPatient() {
        try {
            registry.addPatient(patient1);
            registry.addPatient(patient1);
        } catch (Exception e) {
            assertEquals(e.getMessage(), "This patient already exists in this registry");
        }
        assertEquals(registry.getPatientRegistry().size(), 1);
    }

    @Test
    void removeNonExistingPatient() {
        try {
            registry.addPatient(patient1);
            registry.removePatient(patient2);
        } catch (Exception e) {
            assertEquals(e.getMessage(), "This patient does not exist in this registry");
        }
        assertEquals(registry.getPatientRegistry().size(), 1);
    }
}
